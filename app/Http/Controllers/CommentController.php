<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function __construct() {}

    public function index(Request $request) {
        return Comment::whereNull('up_comment_id')->get();
    }

    public function store(Request $request) {
        $comment = new Comment();
        $comment->body = $request->body;
        $comment->author_id = $request->userId;
        $comment->up_comment_id = $request->previusId;
        $comment->save();

        return Comment::where('id',$comment->up_comment_id)->get();
    }
}
