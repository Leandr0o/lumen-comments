<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConnectionLog extends Model
{
    public $timestamps= false;
}