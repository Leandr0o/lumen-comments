<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class Comment extends Model
{
    public $timestamps= false;
    use SoftDeletes, Uuids;
    protected $with = ['author','subComments'];

    public function author() {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function subComments(){
        return $this->hasMany(Comment::class, 'up_comment_id', 'id');
    }
}